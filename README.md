# ics-ans-role-mtca

Proof of concept using ansible to interact with an inventory of mTCA crates

This repository contains a set of ansible tasks:

## MCH related (via telnet)
1) `show_fru`

    -> run show_fru command to list the cards

2) `power_cycle_amc`

    -> automatically identify the AMC cards in place and power cycle them

## CPU related
1) `reboot_cpu`

    -> perform reboot on the CPU and wait it boot up back

2) `lspci_evr`

    -> perform **lspci** command with grep Xilinx in order to check if EVR card is ok

3) `ls_dev_sis`

    -> perform **ls /dev/sis*** in order to check if Struck cards are ok

4) `check_amc_fw_version`

    -> perform **sis8300drv_fwver** command in order to get AMC firmware version

## Combined task with repeat parameter
`reboot_check_cards`

-> perform a CPU reboot followed by lspci_evr and ls_dev_sis with a `repeat` parameter (default value = 1) to say how many times it should be performed 


### Custom BPM IOC configuration of systemd services
`config_systemd`

-> configure the IOCs deployed on the CPU to initialize in sequence one by one,
avoiding CPU usage peak which could hang the CPU (BPM IOC case).

# Examples

Make sure you have ansible installed (`yum install ansible` on CentOS) and ***ssh*** connection to the hosts you are going to work with.

- how to run the entire playbook:
```bash
ansible-playbook ics-ans-role-mtca/playbook.yml -k -K -i ics-ans-role-
mtca/mtca_inventory -l pbi-bpm06-mtca-ioc01.cslab.esss.lu.se
```
where **-l** option is to limit the hosts target (it can be a single hostname or a group of hosts defined on the inventory file, such as `bpm_lab` for example)

- how to run a specific tagged task (**-t** option):
```bash
ansible-playbook ics-ans-role-mtca/playbook.yml -k -K -i ics-ans-role-mtca/mtca_inventory -l pbi-bpm06-mtca-ioc01.cslab.esss.lu.se -t show_fru
```

- how to pass the repeat parameter when running the combined task (**-e** option):
```bash
ansible-playbook ics-ans-role-mtca/playbook.yml -k -K -i ics-ans-role-mtca/mtca_inventory -l bpm_lab -t reboot_check_cards -e 'repeat=10'
```

## Output example:
```bash
ansible-playbook ics-ans-role-mtca/playbook.yml -k -K -i ics-ans-role-mtca/mtca_inventory -l pbi-bpm06-mtca-ioc01.cslab.esss.lu.se -t ls_dev_sis,lspci_evr
SSH password:
BECOME password[defaults to SSH password]:

PLAY [all] ****************************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************************
ok: [pbi-bpm06-mtca-ioc01.cslab.esss.lu.se]

TASK [ics-ans-role-mtca : include_tasks] **********************************************************************************
included: /home/julianomurari/gitlab/configs/ics-ansible/ics-ans-role-mtca/tasks/lspcievr.yml for pbi-bpm06-mtca-ioc01.cslab.esss.lu.se

TASK [ics-ans-role-mtca : check if EVR card shows up] *********************************************************************
changed: [pbi-bpm06-mtca-ioc01.cslab.esss.lu.se]

TASK [ics-ans-role-mtca : debug] ******************************************************************************************
ok: [pbi-bpm06-mtca-ioc01.cslab.esss.lu.se] => {
    "lspcixilinx.stdout": "0e:00.0 Signal processing controller: Xilinx Corporation Device 7011"
}

TASK [ics-ans-role-mtca : include_tasks] **********************************************************************************
included: /home/julianomurari/gitlab/configs/ics-ansible/ics-ans-role-mtca/tasks/lsdevsis.yml for pbi-bpm06-mtca-ioc01.cslab.esss.lu.se

TASK [ics-ans-role-mtca : check if Struck cards are recognized by OS] *****************************************************
changed: [pbi-bpm06-mtca-ioc01.cslab.esss.lu.se]

TASK [ics-ans-role-mtca : debug] ******************************************************************************************
ok: [pbi-bpm06-mtca-ioc01.cslab.esss.lu.se] => {
    "lsdevsis.stdout_lines": [
        "/dev/sis8300-5",
        "/dev/sis8300-6"
    ]
}

PLAY RECAP ****************************************************************************************************************
pbi-bpm06-mtca-ioc01.cslab.esss.lu.se : ok=7    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

