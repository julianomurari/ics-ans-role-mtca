# Script to set boot order to network

## Check if the efivars are mounted - step needed for ESS Linux
if [ -d /sys/firmware/efi/efivars ]; then
    if [ -z `ls /sys/firmware/efi/efivars` ]; then
        echo "Mouting efivars..."
        mount -t efivarfs efivarfs /sys/firmware/efi/efivars
    fi
fi

## 0. Check if efibootmgr is available and working
EFIBOOTMGRSUPPORT=`efibootmgr 2>&1 | grep "not supported"`
if [ "$EFIBOOTMGRSUPPORT" != '' ]; then
    echo "EFI variables are not supported on this system. Not possible to change the boot order"
    exit
fi

## 1. Check the number of processors (depending on the CCT cpu model, the configuration is different)
CPUS=`nproc --all`

if [ $CPUS = '4' ]; then
## 2. Get network interface for older for older model
    echo "Concurrent Old Model CPU"
    BOOTNET=`efibootmgr | grep "EFI Network 1" | cut -f1 -d "*" | cut -f2 -d "t"`
## 3. Get network interface for newer model
else
    echo "Concurrent New Model CPU"
    BOOTNET=`efibootmgr | grep "Backplane"  | head -n 1 | cut -f1 -d "*" | cut -f2 -d "t"`
fi

# 4. Set new order
BOOTORDER=`efibootmgr | grep "BootOrder" | cut -f2 -d " "`
NEWBOOTORDER=$BOOTNET",""${BOOTORDER//$BOOTNET','}"
efibootmgr -o $NEWBOOTORDER

echo "Boot order changed to have network as first option"
